package com.prueba.despegar.Page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.prueba.despegar.Base.Base;

public class despegarPage extends Base {

	By locatorOrigen=By.xpath("//*[@id=\"searchbox-sbox-box-packages\"]/div/div/div[3]/div[2]/div[2]/div[1]/div/div/div/input");
	By locatorDestino= By.xpath("//input[@class='input-tag sbox-main-focus sbox-destination sbox-secondary sbox-places-second places-inline']");
	By locatorFechaida= By.xpath("/html/body/div[5]/div/div[5]/div[1]/div[4]/span[5]/span[1]");
	By locatorFechasIda=By.cssSelector("#searchbox-sbox-box-packages > div > div > div.sbox-mobile-body > div.sbox-row.-wrap.-row-bottom > div.sbox-row.-wrap.sbox-dates-container.-mb3-m.-mb4-s.-vhh-dates-container.-mh3-l > div.sbox-dates-row.sbox-row > div.sbox-3-input.-md.sbox-3-validation.-top-right.-icon-left.sbox-dates-input.sbox-checkin-container.sbox-datein-container > div > input");
	By locatorFechasvuelta=By.xpath("//*[@id=\"searchbox-sbox-box-packages\"]/div/div/div[3]/div[2]/div[3]/div[2]/div[2]/div");
	By locatorFechavuelta=By.xpath("/html/body/div[5]/div/div[5]/div[1]/div[4]/span[7]/span[1]");
	By locatorMedellin=By.xpath("/html/body/div[12]/div/div[1]/ul/li[1]/span");
	By locatorDestinoBogota=By.xpath("/html/body/div[12]/div/div/ul/li");
	By locatorBuscar=By.cssSelector("#searchbox-sbox-box-packages > div > div > div.sbox-mobile-body > div.sbox-row.-wrap.-row-bottom > div.sbox-button.-ml3-l > div > a > em");



    By locatorSiguiente=By.cssSelector("#pricebox-wc-910149 > div > div.hf-cluster-pricebox.hf-robot-pricebox.-fpv5 > ul > div > li > a");
	public despegarPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	public void seleccionaPaqueteTuristico() throws InterruptedException {
		clear(locatorOrigen);
		type("Mede",locatorOrigen);
		tiempoEspera(10).until(ExpectedConditions.visibilityOfElementLocated(locatorMedellin));
		click(locatorMedellin);
		type("Bogot�, Bogot� D.C., Colombia",locatorDestino);
		tiempoEspera(10).until(ExpectedConditions.visibilityOfElementLocated(locatorDestinoBogota));
		click(locatorDestinoBogota);
		tiempoEspera(10).until(ExpectedConditions.visibilityOfElementLocated(locatorFechasvuelta));
		clickEjecutor(locatorFechasvuelta);
		tiempoEspera(10).until(ExpectedConditions.visibilityOfElementLocated(locatorFechavuelta));
		clickEjecutor(locatorFechavuelta);
		tiempoEspera(10).until(ExpectedConditions.visibilityOfElementLocated(locatorFechasIda));
		clickEjecutor(locatorFechasIda);
		tiempoEspera(10).until(ExpectedConditions.visibilityOfElementLocated(locatorFechaida));
		clickEjecutor(locatorFechaida);
		tiempoEspera(10).until(ExpectedConditions.visibilityOfElementLocated(locatorBuscar));
		click(locatorBuscar);
		
		
	}
	
	
	public void seleccionaViaje() throws InterruptedException {
		Thread.sleep(10000);
	}
	
	
}
