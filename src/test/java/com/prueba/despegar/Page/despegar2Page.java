package com.prueba.despegar.Page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.prueba.despegar.Base.Base;

import cucumber.api.DataTable;

public class despegar2Page extends Base{

	By locatorSiguiente= By.xpath("//*[@id=\"hf-buy-button\"]/em");
	By locatorSiguiente2=By.xpath("//*[@id=\"clusters\"]/span[1]/span/cluster/div/div/div[2]/fare/span/span/div/buy-button/a/div");
	By locatorComprar=By.xpath("//*[@id=\"pricebox-overlay\"]/div[1]/div/button/em");
	By locatorPSE=By.xpath("//*[@id=\"checkout-content\"]/div[1]/payment-component/payment-method-selector/div/ul/li[4]/p/label/i");
	By locatorBanco= By.id("card-selector-0");
	By locatorNombreFactura=By.xpath("//*[@id=\"invoice-first-name-0\"]");
	By locatorApellidoFactura= By.id("invoice-last-name-0");
	By locatorCedulaFactura=By.id("invoice-fiscal-identification-number-0");
	By locatorDireccionFactura= By.id("77em9s2cwqtduoegegfd8k");
	By locatorNombrePasajero1=By.xpath("/html/body/div[2]/div/div/app/checkout-form/div/form-component/form/div[1]/travelers/div/ul/li/ul/li[1]/traveler/div/div[2]/div/div[1]/traveler-first-name-input/div/div/input-component-v2/div/div/div/input");
	By locatorApellidoPasajero1=By.xpath("/html/body/div[2]/div/div/app/checkout-form/div/form-component/form/div[1]/travelers/div/ul/li/ul/li[1]/traveler/div/div[2]/div/div[1]/traveler-last-name-input/div/div/input-component-v2/div/div/div/input");
	By locatorCedulaPasajero1=By.id("traveler-identification-number-0");
	By locatorDiaPasajero1=By.name("formData.travelers[0].birthdate.day");
	By locatorMesPasajero1=By.name("formData.travelers[0].birthdate.month");
	By locatorAņoPasajero1=By.name("formData.travelers[0].birthdate.year");
	
	By locatorNamePasajero2=By.xpath("/html/body/div[2]/div/div/app/checkout-form/div/form-component/form/div[1]/travelers/div/ul/li/ul/li[2]/traveler/div/div[2]/div/div[1]/traveler-first-name-input/div/div/input-component-v2/div/div/div/input");
	By locatorApellidoPasajero2=By.xpath("/html/body/div[2]/div/div/app/checkout-form/div/form-component/form/div[1]/travelers/div/ul/li/ul/li[2]/traveler/div/div[2]/div/div[1]/traveler-last-name-input/div/div/input-component-v2/div/div/div/input");
	By locatorCedulaPasajero2=By.id("traveler-identification-number-1");
	By locatorDiaPasajero2=By.name("formData.travelers[1].birthdate.day");
	By locatorMesPasajero2=By.name("formData.travelers[1].birthdate.month");
	By locatorAņoPasajero2=By.name("formData.travelers[1].birthdate.year");
	public despegar2Page(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	
	public void seleccionDatos(DataTable datos) {
		desplazamiento(1500);
		tiempoEspera(10).until(ExpectedConditions.visibilityOfElementLocated(locatorSiguiente));
		clickEjecutor(locatorSiguiente);
		desplazamiento(800);
		tiempoEspera(10).until(ExpectedConditions.visibilityOfElementLocated(locatorSiguiente2));
		clickEjecutor(locatorSiguiente2);
		tiempoEspera(10).until(ExpectedConditions.visibilityOfElementLocated(locatorComprar));
		click(locatorComprar);
		tiempoEspera(10).until(ExpectedConditions.visibilityOfElementLocated(locatorPSE));
		click(locatorPSE);
		desplazamiento(500);
		selecDropdownList(locatorBanco,"Bancolombia");
		desplazamiento(500);
	    java.util.List<java.util.List<String>> listaDatos = datos.asLists(String.class);
		type(listaDatos.get(1).get(0),locatorNombreFactura);
		type(listaDatos.get(3).get(0),locatorApellidoFactura);
		type(listaDatos.get(4).get(0),locatorCedulaFactura);
		desplazamiento(900);
		type(listaDatos.get(0).get(0),locatorNombrePasajero1);
		type(listaDatos.get(3).get(0),locatorApellidoPasajero1);
		type(listaDatos.get(5).get(0),locatorCedulaPasajero1);
		selecDropdownList(locatorDiaPasajero1,"20");
		selecDropdownList(locatorMesPasajero1,"2");
		selecDropdownList(locatorAņoPasajero1,"2008");
		desplazamiento(600);
		type(listaDatos.get(2).get(0),locatorNamePasajero2);
		type(listaDatos.get(3).get(0),locatorApellidoPasajero2);
		type(listaDatos.get(6).get(0),locatorCedulaPasajero2);
		selecDropdownList(locatorDiaPasajero2,"20");
		selecDropdownList(locatorMesPasajero2,"2");
		selecDropdownList(locatorAņoPasajero2,"2008");
		desplazamiento(1200);
		
		
		
		
	}


	
}
