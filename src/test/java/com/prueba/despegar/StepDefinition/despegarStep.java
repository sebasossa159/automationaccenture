package com.prueba.despegar.StepDefinition;

import org.openqa.selenium.WebDriver;

import com.gargoylesoftware.htmlunit.javascript.host.Map;
import com.prueba.despegar.Page.despegarPage;
import com.sun.tools.javac.util.List;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


public class despegarStep {
	
	private WebDriver driver;
    private despegarPage despegaPage;

	@Given("^Which selects the origin and destination with their respective dates$")
	public void which_selects_the_origin_and_destination_with_their_respective_dates() throws Exception {
	    despegaPage=new despegarPage(driver);
	    driver= despegaPage.chormeDriverConnection();
	    driver.manage().window().maximize();
	    despegaPage.visit("https://www.despegar.com.co/");
	    despegaPage.seleccionaPaqueteTuristico();
	}


	@When("^select the flight$")
	public void select_the_flight() throws Exception {
	   despegaPage.seleccionaViaje();
	}

}
